# Dots Lisp

An implementation of Lisp/Scheme in C#. A fun project to work on something algorithmic for a longer period of time, done in a Randori-styled [Coding Dojo](http://codingdojo.org/WhatIsCodingDojo/), several sessions a 4 hours.

## Installation

This is a .NET Core 2.0 project. The programming language is C# 7.1 (Visual Studio 2017). [xUnit](https://xunit.github.io/) and [FluentAssertions](https://fluentassertions.com/) are provided as dependencies.

* Verify your .NET Core installation with `dotnet --version`.
* Download the required dependency with `dotnet restore`.
* Run `dotnet test` to run the tests.

## Design

![Lisp, Ideas, Elements](DotsLisp_Design.jpg)

## Open questions

* Do we want to separate tokens on certain combinations of characters (e.g. `1"`)?

## Sessions

### 1: 2019-02-12

* 1h ... collect ideas, sketch design elements.
* 1h ... create guiding test.
* 1h ... build `Display`.
* 1h ... evaluate literals (string, number, boolean) tree nodes.

Developer Joke: [How To Build A Horse With Programming](https://blog.toggl.com/build-horse-programming/)

Retrospective

* I liked it ||||||
* commit on each change +
* commit on each change made me understand steps
* Capture Output!
* double close streams issue (is a shame)
* rotation helpful
* moving seats +
* algorithmic +, outside of usual
* 5' is short rotation
* extra keyboard +
* worked as group -> faster and more ideas
* remind Peter that he is not navigator
* Lisp/functional language is nice

### 2: 2019-02-26

* 1,5h ... evaluate simple and nested add-function.
* 2,5h ... parse literals (string, number, boolean) and add-function into tree nodes.

Developer Joke: [Git the Princess!](https://toggl.com/programming-princess/). How to save the Princess in 8 programming languages.

Retrospective

* exhausting
* rotating keeps me fresh
* nicely algorithmic today
* more complex than last time
* I missed last time, hard to get into it
* nice algorithmic challenge
* good we tried generics approach

### 3: 2019-03-12

* 1,0h ... parse nested add-functions into tree nodes.
* 2,5h ... tokenise content into tokens.
* 0,5h ... integrate and finish `Main`.

Developer Joke: [LISP-optimized keyboard project](https://twitter.com/nikitonsky/status/653524568750080000).

Retrospective

* I liked it |||||
* end 2 end test evaluates our architecture
* good progress, success!
* I was lost in beginning
* we wrote some nice code
* refactoring was helpful
* we tried TDD
* skipped test helped us to get started again

### 4: 2019-03-26

* 3h ... finishing tokenizing string variants and cleanup.
* 1h ... start if-function.

Developer Joke: [LISP is ugly and confusing](https://www.reddit.com/r/ProgrammerHumor/comments/9ocqiw/lisp_is_ugly_and_confusing/).

Retrospective

* timer on mobile phone is broken. Maybe use [mob timer](https://www.timeanddate.com/timer/)
* hard to keep focused today
* liked the State Machine, something new |||
* learned the `IEnumerator`, educational |||
* we got rid of working with indexes
* used delegates for real |||
* we spent much time in cleaning up ||
* we would like to see the source of C#, should be possible with .NET core (Tomas will look it up)
* session was relaxing
* good we had the whole session, better than presenting ||
* we need more breaks, today there was only one break
* fun and we make it more fun

### 5: 2019-04-09

* 1h ... fix race condition in capture system output.
* 2h ... evaluation of string if-function and parsing
* 1h ... fight generic of multiple return types

Developer Joke: [Git the Dragon](https://blog.toggl.com/kill-dragon-comic/).

Retrospective

* at start it looked really good and then we got lost
* I learned about lock function ||||
* we struggled and did not move forward
* parsing got much messier than before
* happy that we move forward with types
* we made a second function!
* new web timer is good ||||
* got reminded to use timer ||
* I was lost with types
* some moments I understood what was going on
* I was lost most of the time :-(
* we deleted the last change :-(

### 6: 2019-04-23

* 1h ... discussed possible approaches
* 0.5h ... first approach, then reverted
* 2h ... parse nested if using rootnode everywhere

Developer Joke: [If Programming Languages were an Essay](http://devhumor.com/media/if-programming-languages-were-an-essay).

Retrospective

* union types -- interesting
* chocolate was appreciated
* partial class used for extensibility
* happy with progress made |||
* code cleaned up ||
* prepared code on branch (followed by discussion) |||
* decision paralysis ||
* possibility to start early was nice |||||
* special hw for commits was missed (big enter)

### 7: 2019-05-07

* 1h ... off topic
* 1h ... if body for boolean
* 2h ... generalized type handling

Developer Joke: [Infinica](https://www.infinica.com/) ;-)

Retrospective

* PO is supporting this ||||||
* like to support PO |||
* team is engaged
* liked generalized types
* liked partial class approach for extension
* do not like partial classes with same names
* `AllTypes` class is more general |||
* improved structure a lot |||
* some conclusion of exercise ||
* code is more readable ||
* new interface forces type structure
* learned a VS refactoring: moving stuff to partial
* It was fun.
* It was a long session.
* Surprised that we used tokens instead of whole string in parser tests.
* Room is fancy!

### License

This work is licensed under a [New BSD License](http://opensource.org/licenses/bsd-license.php), see `license.txt` in repository.
