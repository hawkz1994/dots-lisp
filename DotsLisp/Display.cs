﻿using System;
using System.Collections.Generic;

namespace DotsLisp
{
    public class Display
    {
        public void Print(IEnumerable<object> values)
        {
            foreach (var value in values)
            {
                Console.WriteLine(GetDisplayValue(value));
            }
        }

        private string GetDisplayValue(object value)
        {
            // TODO 4 (PK) "extensibility": Use AllTypes similar for parsing also for display.
            if (value is string)
            {
                return ConvertString(value);
            }
            else if (value is bool)
            {
                return ConvertBoolean(value);
            }
            else if (value is int)
            {
                return Convert.ToString(value);
            }
            else
            {
                throw new NotImplementedException($"Unknown type {value.GetType().Name}");
            }
        }

        private string ConvertBoolean(object value)
        {
            var boolValue = (bool)value;
            return (boolValue ? "#t" : "#f");
        }

        private string ConvertString(object value)
        {
            return $"\"{value}\"";
        }

    }
}
