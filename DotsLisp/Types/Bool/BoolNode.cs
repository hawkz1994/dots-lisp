﻿using DotsLisp.Nodes;

namespace DotsLisp.Types.Bool
{
    // TODO 3 (PK) "duplication/OO": can we reduce to a single `class LiteralNode<T> : INode<T>` with field T.

    public class BoolNode : INode<bool>
    {
        private readonly bool value;

        public BoolNode(bool value)
        {
            this.value = value;
        }

        public bool Eval()
        {
            return value;
        }
    }
}
