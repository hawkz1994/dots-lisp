﻿using System;

namespace DotsLisp.Nodes
{
    // TODO 2 (Philip) "naming": Rename all partial RootNodes files to e.g. `NotBoolRootNode`.
    // TODO 2 (PK) "naming": Should `partial` be in the name of all files containing partial RootNode?
    public partial class RootNode : INode<object>
    {
        public virtual bool IsBoolNode()
        {
            return false;
        }

        public virtual INode<bool> ToBoolNode()
        {
            throw new NotImplementedException("Expected bool.");
        }
    }
}
