﻿using DotsLisp.Nodes;

namespace DotsLisp.Types.Bool
{
    public class BoolRootNode : RootNode
    {
        // TODO (PK) "style": Make all fields readonly if possible.
        private INode<bool> boolNode;

        public BoolRootNode(INode<bool> boolNode)
        {
            this.boolNode = boolNode;
        }

        public override object Eval()
        {
            return boolNode.Eval();
        }

        public override bool IsBoolNode()
        {
            return true;
        }

        public override INode<bool> ToBoolNode()
        {
            return boolNode;
        }
    }
}
