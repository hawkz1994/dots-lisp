﻿using DotsLisp.Nodes;
using System;

namespace DotsLisp.Types.Bool
{
    public class BoolType : IType
    {
        public bool CanParse(string token)
        {
            return token == "#t" || token == "#f";
        }

        public RootNode ParseNode(string token)
        {
            if (!CanParse(token))
            {
                throw new ArgumentException($"Token {token} is not bool");
            }

            return new BoolRootNode(new BoolNode(token == "#t"));
        }
    }
}
