﻿using DotsLisp.Nodes;
using DotsLisp.Types.Bool;
using DotsLisp.Types.Int;
using DotsLisp.Types.String;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotsLisp.Types
{
    public class AllTypes
    {
        private readonly List<IType> types = new List<IType>
        {
            // TODO 1 "extensibility": Find all implementations inside assembly with reflection.
            new IntType(),
            new StringType(),
            new BoolType()
        };

        public RootNode ParseSingleToken(string token)
        {
            var type = types.FirstOrDefault(t => t.CanParse(token));
            if (type != null)
            {
                return type.ParseNode(token);
            }
            throw new ArgumentException($"Token {token} has unknown type.");
        }
    }
}
