﻿using DotsLisp.Nodes;

namespace DotsLisp.Types
{
    interface IType
    {
        bool CanParse(string token);

        RootNode ParseNode(string token);
    }
}
