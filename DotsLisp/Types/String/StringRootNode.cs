﻿using DotsLisp.Nodes;
using System;

namespace DotsLisp.Types.String
{
    public class StringRootNode : RootNode
    {
        private INode<string> stringNode;

        public StringRootNode(INode<string> stringNode)
        {
            this.stringNode = stringNode;
        }

        public override object Eval()
        {
            return stringNode.Eval();
        }

        public override bool IsStringNode()
        {
            return true;
        }

        public override INode<string> ToStringNode()
        {
            return stringNode;
        }
    }
}
