﻿using DotsLisp.Nodes;

namespace DotsLisp.Types.String
{
    public class StringNode : INode<string>
    {
        private readonly string value;

        public StringNode(string value)
        {
            this.value = value;
        }

        public string Eval()
        {
            return value;
        }
    }
}
