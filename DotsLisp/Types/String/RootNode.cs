﻿using System;

namespace DotsLisp.Nodes
{
    public partial class RootNode : INode<object>
    {
        public virtual bool IsStringNode()
        {
            return false;
        }

        public virtual INode<string> ToStringNode()
        {
            throw new NotImplementedException("Expected string.");
        }
    }
}
