﻿using DotsLisp.Nodes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotsLisp.Types.String
{
    public class StringType : IType
    {
        public bool CanParse(string token)
        {
            return token.StartsWith("\"") && token.EndsWith("\"") && token.Length >= 2;
        }

        public RootNode ParseNode(string token)
        {
            if (!CanParse(token))
            {
                throw new ArgumentException($"Token {token} is not string");
            }

            var clearedString = string.Empty;
            if (token.Length > 2)
            {
                clearedString = token.Substring(1, token.Length - 2);
            }

            return new StringRootNode(new StringNode(clearedString));
        }
    }
}
