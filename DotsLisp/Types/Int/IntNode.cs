﻿using DotsLisp.Nodes;

namespace DotsLisp.Types.Int
{
    public class IntNode : INode<int>
    {
        private readonly int value;

        public IntNode(int value)
        {
            this.value = value;
        }

        public int Eval()
        {
            return value;
        }
    }
}
