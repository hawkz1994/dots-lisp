﻿using DotsLisp.Nodes;
using System;

namespace DotsLisp.Types.Int
{
    public class IntType : IType
    {
        public bool CanParse(string token)
        {
            return int.TryParse(token, out _);
        }

        public RootNode ParseNode(string token)
        {
            if (!CanParse(token))
            {
                throw new ArgumentException($"Token {token} is not number");
            }

            return new IntRootNode(new IntNode(int.Parse(token)));
        }
    }
}
