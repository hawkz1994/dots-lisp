﻿using DotsLisp.Nodes;

namespace DotsLisp.Types.Int
{
    public class IntRootNode : RootNode
    {
        private INode<int> intNode;

        public IntRootNode(INode<int> intNode)
        {
            this.intNode = intNode;
        }

        public override object Eval()
        {
            return intNode.Eval();
        }

        // TODO "style": make it a property
        // also make all other Is* methods (3*2+3 a property)
        public override bool IsIntNode()
        {
            return true;
        }

        public override INode<int> ToIntNode()
        {
            return intNode;
        }
    }
}
