﻿using System;

namespace DotsLisp.Nodes
{
    public partial class RootNode
    {
        public virtual bool IsIntNode()
        {
            return false;
        }

        public virtual INode<int> ToIntNode()
        {
            throw new NotImplementedException("Expected int.");
        }
    }
}
