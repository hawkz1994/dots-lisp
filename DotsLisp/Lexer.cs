﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotsLisp
{
    delegate void CharHandler(char character);

    public class Lexer
    {
        private readonly string content;
        private readonly StringBuilder currentToken;
        private readonly List<string> tokens;

        private char previousCharacter;

        private CharHandler TokenizeCharacter;

        public Lexer(string content)
        {
            this.content = content;
            this.currentToken = new StringBuilder();
            this.tokens = new List<string>();

            this.TokenizeCharacter = AppendToCurrentToken;
        }

        public IEnumerable<string> Tokenize()
        {
            foreach (char character in content)
            {
                // TODO (PK) "OO/SoC": introduce wrapper class for char character with logic which is character based
                // will separate out the character based logic which is low level from Lexer
                // e.g. class LexerCharacter with character.IsBracket(), previousCharacter.IsEscape() etc.
                TokenizeCharacter(character);
                previousCharacter = character;
            }

            FinalizeCurrentToken();

            return tokens;
        }

        private void AppendToCurrentToken(char character)
        {
            if (IsBracket(character))
            {
                FinalizeCurrentToken();
                Append(character);
                FinalizeCurrentToken();
            }
            else if (IsWhiteSpace(character))
            {
                FinalizeCurrentToken();
            }
            else
            {
                Append(character);
            }

            if (IsQuote(character))
            {
                TokenizeCharacter = AppendString;
            }
        }

        private void AppendString(char character)
        {
            Append(character);

            if (IsQuote(character) && !IsAfterBackslash())
            {
                TokenizeCharacter = AppendToCurrentToken;
            }
        }

        private bool IsAfterBackslash()
        {
            return previousCharacter == '\\';
        }

        private bool IsQuote(char character)
        {
            return character == '"';
        }

        private bool IsBracket(char character)
        {
            return character == '(' || character == ')';
        }

        private bool IsWhiteSpace(char character)
        {
            return String.IsNullOrWhiteSpace(character.ToString());
        }

        private void Append(char character)
        {
            currentToken.Append(character);
        }

        private void FinalizeCurrentToken()
        {
            if (currentToken.Length > 0)
            {
                tokens.Add(currentToken.ToString());
                currentToken.Clear();
            }
        }
    }
}
