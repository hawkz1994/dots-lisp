﻿namespace DotsLisp.Nodes
{
    public class IfNode<T> : INode<T>
    {
        private readonly INode<bool> condition;
        private readonly INode<T> trueStatement;
        private readonly INode<T> falseStatement;

        public IfNode(INode<bool> condition, INode<T> trueStatement, INode<T> falseStatement)
        {
            this.condition = condition;
            this.trueStatement = trueStatement;
            this.falseStatement = falseStatement;
        }
        public T Eval()
        {
            if (condition.Eval())
            {
                return trueStatement.Eval();
            }
            else
            {
                return falseStatement.Eval();
            }
        }
    }
}
