﻿using System;

namespace DotsLisp.Nodes
{
    public partial class RootNode : INode<object>
    {
        public virtual object Eval()
        {
            throw new ArgumentException("No node set");
        }
    }
}
