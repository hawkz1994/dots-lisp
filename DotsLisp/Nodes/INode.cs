﻿namespace DotsLisp.Nodes
{
    public interface INode<T>
    {
        T Eval();
    }
}
