﻿using System.Collections.Generic;
using System.Linq;

namespace DotsLisp.Nodes
{
    public class AddNode : INode<int>
    {
        private readonly IEnumerable<INode<int>> arguments;

        public AddNode(IEnumerable<INode<int>> arguments)
        {
            this.arguments = arguments;
        }

        public int Eval()
        {
            return arguments.Sum(arg => arg.Eval());
        }
    }
}
