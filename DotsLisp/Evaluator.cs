﻿using DotsLisp.Nodes;

namespace DotsLisp
{
    public class Evaluator
    {
        public T Eval<T>(INode<T> node)
        {
            return node.Eval();
        }
    }
}
