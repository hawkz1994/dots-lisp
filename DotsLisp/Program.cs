﻿using DotsLisp.Nodes;
using System;
using System.Collections.Generic;

namespace DotsLisp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // TODO (PK) "feature": command line argument handling, help message
            var path = args[0];
            var content = ReadFile(path);
            var tokens = Tokenize(content);
            var rootNode = Parse(tokens);
            var results = Evaluate(rootNode);
            Display(results);
        }

        private static string ReadFile(string path)
        {
            // TODO (PK) "error handling": handle missing file
            return System.IO.File.ReadAllText(path);
        }

        private static IEnumerable<string> Tokenize(string content)
        {
            var lexer = new Lexer(content);
            return lexer.Tokenize();
        }

        private static RootNode Parse(IEnumerable<string> tokens)
        {
            var parser = new Parser();
            return parser.Parse(tokens);
        }

        private static IEnumerable<object> Evaluate(RootNode rootNode)
        {
            var evaluator = new Evaluator();
            return new List<object> { evaluator.Eval(rootNode) };
        }

        private static void Display(IEnumerable<object> results)
        {
            var display = new Display();
            display.Print(results);
        }
    }
}
