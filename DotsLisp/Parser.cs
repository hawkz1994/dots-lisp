﻿using DotsLisp.Nodes;
using DotsLisp.Types;
using DotsLisp.Types.Bool;
using DotsLisp.Types.Int;
using DotsLisp.Types.String;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DotsLisp
{
    public class Parser
    {
        private AllTypes types = new AllTypes();

        // TODO (PK) "feature": return IEnumerable<RootNode>
        public RootNode Parse(IEnumerable<string> allTokens)
        {
            var tokens = allTokens.GetEnumerator();
            tokens.MoveNext();

            return Parse(tokens);
        }

        private RootNode Parse(IEnumerator<string> tokens)
        {
            var token = tokens.Current;
            if (!IsFunction(token))
            {
                tokens.MoveNext();
                return types.ParseSingleToken(token);
            }

            return ParseFunction(tokens);
        }

        private bool IsFunction(string token)
        {
            return token == "(";
        }

        private RootNode ParseFunction(IEnumerator<string> tokens)
        {
            // TODO 5 (PK) "SoC/OO/extensibility": Create namespace `Functions` with `AddParser` and `AddNode`, `IfParser` and IfNode
            // and a `FunctionParser` class which will lookup the function  based on the function name.
            // `IFunctionParser` will be the interface for the `AddParser` and `IfParser`, similar to Types.
            // Then we can add more functions as we like.

            tokens.MoveNext(); // Opening bracket
            var functionName = tokens.Current;
            tokens.MoveNext();
            if (functionName == "+")
            {
                var addNode = ParseAddFunction(tokens);
                tokens.MoveNext(); // Closing bracket
                return addNode;
            }
            else if (functionName == "if")
            {
                var ifNode = ParseIfFunction(tokens);
                // TODO "syntax check": check for closing bracket
                tokens.MoveNext(); // closing brack
                return ifNode;

            }
            throw new NotImplementedException($"Unknown function: {functionName}");
        }

        private RootNode ParseAddFunction(IEnumerator<string> tokens)
        {
            var arguments = new List<INode<int>>();
            while (tokens.Current != ")")
            {
                var node = Parse(tokens);
                arguments.Add(node.ToIntNode());
            }
            return new IntRootNode(new AddNode(arguments));
        }

        private RootNode ParseIfFunction(IEnumerator<string> tokens)
        {
            var condition = Parse(tokens).ToBoolNode();
            var trueStatement = Parse(tokens);
            var falseStatement = Parse(tokens);

            if (trueStatement.IsIntNode())
            {
                return new IntRootNode(new IfNode<int>(condition, trueStatement.ToIntNode(), falseStatement.ToIntNode()));
            }

            if (trueStatement.IsStringNode())
            {
                return new StringRootNode(new IfNode<string>(condition, trueStatement.ToStringNode(), falseStatement.ToStringNode()));
            }

            if (trueStatement.IsBoolNode())
            {
                return new BoolRootNode(new IfNode<bool>(condition, trueStatement.ToBoolNode(), falseStatement.ToBoolNode()));
            }

            throw new NotImplementedException("Unknown type for if statement (trueStatement)");

            // TODO "syntax check": diferent type of exceptions: coding error, lisp syntax error, not implemented
            // different exceptions all over the code. Search for `throw new`.
        }
    }
}
