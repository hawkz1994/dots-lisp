﻿using FluentAssertions;
using System;
using System.IO;
using Xunit;

namespace DotsLisp.Tests
{
    public class DotsLispTests
    {

        // TODO (PK) "tests": remove this useless test
        [Fact]
        public void InfrasctructureShouldWork()
        {
            var isWorking = true;
            isWorking.Should().BeTrue();
        }

        [Fact]
        public void GuidingTestSimpleAddExpression()
        {
            var inputFileName = @"TestData\SimpleAddExpression.txt";
            Action main = () => Program.Main(new string[] { inputFileName });

            var output = Capture.ConsoleOutput(main);

            output.Should().Be("6\r\n");
        }
    }
}
