﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DotsLisp.Tests
{
    public class DisplayTests
    {
        [Theory]
        [InlineData(3, "3")]
        [InlineData("Hello world!", "\"Hello world!\"")]
        [InlineData(true, "#t")]
        public void ShouldDisplaySingleValue(object value, string result)
        {
            var display = new Display();
            var values = new List<object> { value };
            Action print = () => display.Print(values);

            var output = Capture.ConsoleOutput(print);

            output.Should().Be($"{result}{Environment.NewLine}");
        }
    }
}
