(define (factorial n)
    (define (factorial-impl n acc)
        (if (= n 0)
            acc
            (factorial-impl (- n 1)
                            (* acc n))))
    (factorial-impl n 1))

(println (factorial 5))
