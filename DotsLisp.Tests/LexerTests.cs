﻿using FluentAssertions;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DotsLisp.Tests
{
    public class LexerTests
    {
        [Fact]
        public void ShouldTokenizeEmptyString()
        {
            // TODO (PK) "tests": move this into InlineData as well
            Lexer lexer = new Lexer(string.Empty);

            var tokens = lexer.Tokenize();

            tokens.Should().BeEmpty();
        }

        [Theory]
        [InlineData("1", new string[] { "1" })]
        [InlineData("11", new string[] { "11" })]
        [InlineData("1 2", new string[] { "1", "2" })]
        [InlineData(" 1\n2\t3  4\r\n5  ", new string[] { "1", "2", "3", "4", "5" })]
        [InlineData("(+ 1 2)", new string[] { "(", "+", "1", "2", ")" })]
        [InlineData("\"a\"", new string[] { "\"a\"" })]
        [InlineData("\"a b\"", new string[] { "\"a b\"" })]
        [InlineData("\"a(b\"", new string[] { "\"a(b\"" })]
        [InlineData("\"a \\\" b\"", new string[] { "\"a \\\" b\"" })]
        public void ShouldTokenizeInput(string content, string[] expectedTokens)
        {
            Lexer lexer = new Lexer(content);

            var tokens = lexer.Tokenize();

            tokens.Should().BeEquivalentTo(expectedTokens);
        }
    }
}
