﻿using DotsLisp.Nodes;
using FluentAssertions;
using Xunit;

namespace DotsLisp.Tests
{
    public class ParserTests
    {
        private Parser parser = new Parser();

        // TODO: Is it right that we use `eval` here? This is not a unit test then.
        // Maybe create expected tree and compare with equals method.

        // TODO (Thomas): should we use a simple Lisp string and use Lexer to get tokens to make it easier?

        [Theory]
        [InlineData("#t", true)]
        [InlineData("#f", false)]
        [InlineData("1", 1)]
        [InlineData("-1", -1)]
        [InlineData("\"Hell World!\"", "Hell World!")]
        public void ShouldParseSingleToken(string token, object expected)
        {
            var tokens = new[] { token };

            var result = parser.Parse(tokens);

            result.Should().BeAssignableTo<RootNode>();
            result.Eval().Should().Be(expected);
        }

        [Fact]
        public void ShouldParseAddWithOneParameter()
        {
            var tokens = new[] { "(", "+", "1", ")" };

            var result = parser.Parse(tokens);

            result.Eval().Should().Be(1);
        }

        [Fact]
        public void ShouldParseNestedAdd()
        {
            var tokens = new[] { "(", "+", "(", "+", "1", "2", ")", "3", ")" };

            var result = parser.Parse(tokens);

            result.Eval().Should().Be(1 + 2 + 3);
        }

        [Fact]
        public void ShouldParseIfStatementWithNumbers()
        {
            // (if #f 3 5)
            var tokens = new[] { "(", "if", "#f", "3", "5", ")" };

            var result = parser.Parse(tokens);

            result.Eval().Should().Be(5);
        }

        [Fact]
        public void ShouldParseIfStatementWithStrings()
        {
            // (if #t "\"hi\"" "\"bye\"")
            var tokens = new[] { "(", "if", "#t", "\"hi\"", "\"bye\"", ")" };

            var result = parser.Parse(tokens);

            result.Eval().Should().Be("hi");
        }

        [Fact]
        public void ShouldParseIfStatementWithBool()
        {
            // (if #t #t #f)
            var tokens = new[] { "(", "if", "#t", "#t", "#f", ")" };

            var result = parser.Parse(tokens);

            result.Eval().Should().Be(true);
        }

        [Fact]
        public void ShouldParseIfStatementWithNestedFunction()
        {
            // (if (if #t #t #f) (+ 1 2) 5)
            var tokens = new[] { "(", "if", "(", "if", "#t", "#t", "#f", ")", "(", "+", "1", "2", ")", "5", ")" };

            var result = parser.Parse(tokens);

            result.Eval().Should().Be(3);
        }
    }
}
