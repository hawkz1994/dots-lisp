﻿using DotsLisp.Nodes;
using DotsLisp.Types.Bool;
using DotsLisp.Types.Int;
using DotsLisp.Types.String;
using FluentAssertions;
using Xunit;

namespace DotsLisp.Tests
{
    public class EvaluatorTests
    {
        private Evaluator evaluator = new Evaluator();

        [Fact]
        public void ShouldEvaluateSingleNumberNode()
        {
            var node = new IntNode(3);

            var result = evaluator.Eval(node);

            result.Should().Be(3);
        }

        [Fact]
        public void ShouldEvaluateSingleStringNode()
        {
            var node = new StringNode("Hello world!");

            var result = evaluator.Eval(node);

            result.Should().Be("Hello world!");
        }

        [Fact]
        public void ShouldEvaluateSingleBoolNode()
        {
            var node = new BoolNode(true);
            var result = evaluator.Eval(node);

            result.Should().Be(true);
        }

        [Fact]
        public void ShouldEvaluateAddNode()
        {
            var a = new IntNode(1);
            var b = new IntNode(3);
            var aPlusB = new AddNode(new[] { a, b });

            var result = evaluator.Eval(aPlusB);

            result.Should().Be(1 + 3);
        }

        [Fact]
        public void ShouldNotEvaluateAddStringNode()
        {
            var node = new StringNode("Hello world!");
            // new AddNode(node); // does not compile -> green
        }

        [Fact]
        public void ShouldEvaluateEmptyAddNode()
        {
            var add = new AddNode(new IntNode[0]);

            var result = evaluator.Eval(add);

            result.Should().Be(0);
        }

        [Fact]
        public void ShouldEvaluateNestedAddition()
        {
            // + (+ a b) c
            var a = new IntNode(1);
            var b = new IntNode(2);
            var aPlusB = new AddNode(new[] { a, b });
            var c = new IntNode(3);
            var aPlusBPlusC = new AddNode(new INode<int>[] { aPlusB, c });

            var result = evaluator.Eval(aPlusBPlusC);

            result.Should().Be(1 + 2 + 3);
        }

        // TODO (PK) "test infrastructure": extract this to toplevel class in test project, make it generic <T>
        private class NeverEvaluatingNode<T> : INode<T>
        {
            public T Eval()
            {
                throw new Xunit.Sdk.TrueException("Should not be called.", false);
            }
        }

        [Fact]
        public void ShouldEvaluateIfFunctionForStringStatements()
        {
            // (if #t "true" "false")
            var condition = new BoolNode(true);
            var trueStatement = new StringNode("\"true\"");
            var falseStatement = new NeverEvaluatingNode<string>();
            var ifNode = new IfNode<string>(condition, trueStatement, falseStatement);

            var result = evaluator.Eval(ifNode);

            result.Should().Be("\"true\"");
        }

        [Fact]
        public void ShouldEvaluateIfFunctionForNumbers()
        {
            // (if #f 3 5)
            var condition = new BoolNode(false);
            var trueStatement = new NeverEvaluatingNode<int>();
            var falseStatement = new IntNode(5);
            var ifNode = new IfNode<int>(condition, trueStatement, falseStatement);

            var result = evaluator.Eval(ifNode);

            result.Should().Be(5);
        }
    }
}
